﻿using System.Drawing;

namespace ConsoleColoring;

public static class AnsiUtilities
{
    public const string Esc = "\u001b";
    public const string Csi = Esc + "[";

    public static string CreateForeground(Color color)
    {
        var codes = new List<byte>
        {
                        38,
                        2,
                        color.R,
                        color.G,
                        color.B
        };
        return $"{Csi}{string.Join(";", codes.Select(c => c.ToString()))}m";
    }

    public static string CreateBackground(Color color)
    {
        var codes = new List<byte>
        {
                        48,
                        2,
                        color.R,
                        color.G,
                        color.B
        };
        return $"{Csi}{string.Join(";", codes.Select(c => c.ToString()))}m";
    }
}